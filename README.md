# Git Workflow


## Main objectives of Git Workflow

* Keep track of history
* Avoid conflicts (merge conflicts)
* Improve team members productivity and sharing solutions
* Keep the evolution of the project
* Control the quality of the code through code reviews and Pull Requests (PRs)

## Branch names schema

In Git Workflow the name of the branches follow a pattern to esasily identify what each branch will do when finished.

In general, there are the "top level" branches, like: "master", "develop", "homolog", "qa", etc. It will depend of the project nature, structure. Beside those, all other branches should follow these rules:

* Use a "prefix"
  * `feat/` for new features
  * `fix/` for bug fixes
  * `chore/` for branches that are not related to the codebase itself, like documentation improvement or adding new patterns to `.gitignore` file.
* After the prefix, if you can identify your branch with some task, use it and only then, a briefly description of the branch objective.

Examples:

* `feat/JRA-123-create-login-page`
* `fix/JIRA-456-remove-duplicate-header`
* `fix/add-save-login-session-checkbox`

This is very important because, in the end, the "Merge" messages that those branches will have will look like:

> Merge pull request #681 from lusiaves/fix/MFW-645-showing-modal-when-no-documents-exist

 
## Merge strategies

In general, top level branches should never receive a commit directly from you. Every merge to "develop" or "master" should be done through Pull Requests or automated pipeline deploys.

## Conventional commits

You should use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). Please, get used to it and apply it to have a good commit message standard accross your project/team.

## Creating Pull Requests

When creating PRs, it's a good idea to use some "tags" in the message. Some examples from Lusiave's project:

>[RFC][JRA-123] Fix modal oppening bug

As you can see above, we have:
* [RFC]: this is a `tag` that shows the branch is Ready For Commit
* [JRA-123] this is another `tag` that identifies the Jira task ID related to this commit
* Fix modal oppening bug: this is the title of the PR message


